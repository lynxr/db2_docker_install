FROM centos:6

MAINTAINER Evgeny Zakharov <jindos.lp@gmail.com>

VOLUME ["/home/john/DB2HOME:/home"]

ENV DB2_INSTALLER v10.5_linuxx64_expc.tar.gz
ENV DB2_INST_DIR /opt/installer
ENV DB2_RESP resp.rsp 
ENV DB2_INSTANCE_DIR /opt/ibm/db2/V10.5/

RUN yum -y install file tar pam.i686 libaio libstdc++.i686
RUN mkdir /opt/installer

COPY ${DB2_RESP} /tmp/${DB2_RESP}
COPY ${DB2_INSTALLER} ${DB2_INST_DIR}/${DB2_INSTALLER}
RUN cd ${DB2_INST_DIR} && tar xzvf ${DB2_INSTALLER} -C ${DB2_INST_DIR} && rm -f ${DB2_INSTALLER}

RUN cd ${DB2_INST_DIR}/expc && \
 ./db2setup -r /tmp/${DB2_RESP} && \
 cat /tmp/db2setup.log

EXPOSE 50000
CMD useradd -m das && useradd -m db2fenc && useradd -m db2inst && \
 cd ${DB2_INSTANCE_DIR}/instance && \
 ./dascrt das && ./db2icrt -u db2fenc -p 50000 db2inst && \
 su - db2inst -c db2start && /bin/bash
